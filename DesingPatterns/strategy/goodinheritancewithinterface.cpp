#include <iostream>

class FlyBehavior {
 public:
  virtual void fly() = 0;
};

class FlyWithWings : public FlyBehavior {
 public:
  void fly() { std::cout << "I'm flying\n"; }
};

class FlyNoWay : public FlyBehavior {
 public:
  void fly() { std::cout << "I can't fly\n"; }
};

/**
 * Base class which is a abstract class
 * */
class Duck {
 public:
  FlyBehavior *flyBehavior;
  void swim() { std::cout << "I'm swimming\n"; }
  void performFly() { flyBehavior->fly(); }
  void setFlyBehavior(FlyBehavior *fb) { flyBehavior = fb; }
};

class CityDuck : public Duck {
 public:
  CityDuck() { flyBehavior = new FlyWithWings; }
  void display() { std::cout << "I'm a city duck\n"; }
};

class RubberDuck : public Duck {
 public:
  RubberDuck() { flyBehavior = new FlyNoWay; }
  void display() { std::cout << "I'm rubber duck\n"; }
};

int main() {
  CityDuck cityDuck;
  cityDuck.display();
  cityDuck.performFly();
  cityDuck.swim();

  RubberDuck rubberDuck;
  rubberDuck.display();
  rubberDuck.performFly();
  rubberDuck.swim();

  // Let's fly the rubber duck anyway, we know it is not right :P
  std::cout << "Changing rubber duck behavior\n";
  rubberDuck.setFlyBehavior(new FlyWithWings);
  rubberDuck.performFly();

  return 0;
}