## Inheritance implementation which performed "bad" in this case

```plantuml
@startuml
class Duck {
    +swim()
    +fly()
    +quack()
}

class CityDuck extends Duck {
    +display()
}

class RubberDuck extends Duck {
    +display()
    +fly()
}
@enduml
```

```
$ ./badinheritance
I'm a city duck
I'm flying
I'm rubber duck
I'm flying
```


## Good implementation of inheritance
```plantuml
@startuml
class Duck {
    +swim()
}

class CityDuck {
    +display()
    +fly()
}

class RubberDuck   {
    +display()
    +fly()
}

Duck <|-- CityDuck : "is a"
Duck <|-- RubberDuck : "is a"
Duck <|-- MoreDucks

class FlyWithWings {
    +fly()
}

class FlyNoWay {
    +fly() {//can't fly}
}

DecoyDuck::fly -> FlyNoWay
FlyNoWay <- RubberDuck::fly

CityDuck::fly -> FlyWithWings
MoreDucks::fly -> TheirFlyingBehavior

@enduml
```

```
$ ./goodinheritance
I'm a city duck
I'm flying
I'm swimming
I'm rubber duck
I can't fly
I'm swimming
```

## Good inheritance implementation and interface
```plantuml
@startuml
abstract class Duck {
    #FlyBehavior flyBehavior
    +swim()
    +virtual display()=0
    +performFly()
    +performQuack()
}

Duck::flyBehavior -> FlyBehavior

class CityDuck extends Duck {
    +flyBehavior
    +display()
    +fly()
}

CityDuck::flyBehavior -> FlyWithWings

class RubberDuck extends Duck {
    +flyBehavior
    +display()
    +fly()
}

RubberDuck::flyBehavior -> FlyNoWay

interface FlyBehavior {
    +virtual fly()=0
}


class FlyWithWings implements FlyBehavior {
    +fly()
}

class FlyNoWay implements FlyBehavior {
    +fly() {//can't fly}
}
@enduml
```

```
$ ./goodinheritancewithinterface
I'm a city duck
I'm flying
I'm swimming
I'm rubber duck
I can't fly
I'm swimming
```