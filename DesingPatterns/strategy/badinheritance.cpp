#include <iostream>

class Duck {
 public:
  void fly() { std::cout << "I'm flying\n"; }
  void swim() { std::cout << "I'm swimming\n"; }
  void quck() { std::cout << "Quack\n"; }
};

class CityDuck : public Duck {
 public:
  void display() { std::cout << "I'm a city duck\n"; }
};

class RubberDuck : public Duck {
 public:
  void display() { std::cout << "I'm rubber duck\n"; }
};

int main() {
  CityDuck cityDuck;
  cityDuck.display();
  cityDuck.fly();

  RubberDuck rubberDuck;
  rubberDuck.display();
  rubberDuck.fly();  // Rubber ducks don't really fly, a fix is required to override this in rubber class

  return 0;
}