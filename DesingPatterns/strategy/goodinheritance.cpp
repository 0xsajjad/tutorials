#include <iostream>
class FlyWithWings {
 public:
  void fly() { std::cout << "I'm flying\n"; }
};

class FlyNoWay {
 public:
  void fly() { std::cout << "I can't fly\n"; }
};

class Duck {
 public:
  void swim() { std::cout << "I'm swimming\n"; }
};

class CityDuck : public Duck {
 public:
  void display() { std::cout << "I'm a city duck\n"; }
  void PerofrmFly() {
    FlyWithWings fly;
    fly.fly();
  }
};

class RubberDuck : public Duck {
 public:
  void display() { std::cout << "I'm rubber duck\n"; }
  void PerformFly() {
    FlyNoWay fly;
    fly.fly();
  }
};

int main() {
  CityDuck cityDuck;
  cityDuck.display();
  cityDuck.PerofrmFly();
  cityDuck.swim();

  RubberDuck rubberDuck;
  rubberDuck.display();
  rubberDuck.PerformFly();
  rubberDuck.swim();

  return 0;
}