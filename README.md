# Tutorials

Source code for all the tutorials available on Youtube.

## Getting Started
Clone the repository using the following command:

```git clone git@gitlab.com:0xsajjad/tutorials.git```

## Build executable
Each design pattern is placed in a directory, to build the application

```
cd <folder>
cmake -B build .
cd build
make
```